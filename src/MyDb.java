
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyDb {

    private static Connection con;
    private static Statement st;

    static {
        try {
            Class.forName("org.sqlite.JDBC");
            //Same hi hai .Ye hi file hai
            //whats the error?
            con = DriverManager.getConnection("jdbc:sqlite:MedicineDB.db");
            st = con.createStatement();

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    /**
     * @param args
     */
    private ResultSet rs;

    public String[][] getData(String querry) {
        int row = 0, col = 0;
        try {
            rs = st.executeQuery(querry);
            ResultSetMetaData rsmd = rs.getMetaData();
            col = rsmd.getColumnCount();
            while (rs.next()) {
                row++;
            }

            //System.out.println("row:" + row + "columns:" + col);
            String s[][] = new String[row][col];

            rs = st.executeQuery(querry);
            int i = 0;
            while (rs.next()) {
                for (int j = 0; j < col; j++) {
                    s[i][j] = rs.getString(j + 1);
                    //System.out.println(s[i][j]);
                }
                i++;
            }

            return s;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void insert(String name, String manufacturer, int cost, String date) {

        try {
            String q = "select count(*) from mIdName where medicineName='" + name + "'";
            rs = st.executeQuery(q);
            int count = Integer.parseInt(rs.getString(1));

            String sqlInsert = "insert into mIdName(medicineName,expiry) values('" + name + "','" + date + "')";
            st.executeUpdate(sqlInsert);
            if (count == 0) {

                sqlInsert = "insert into mDetails values('" + name + "','" + manufacturer + "'," + cost + "," + 0 + ")";
                st.executeUpdate(sqlInsert);
            }
            updateStock();
            /*sqlInsert = "select count(*),medicineName from mIdName group by medicineName order by medicineId";
             //q="select * from mDetails";
             rs = st.executeQuery(sqlInsert);
            
             //ResultSet lrs=st.executeQuery(q);
             int row = 0;
             /*while(rs.next())
             row++;
             System.out.println(row);
             while (rs.next()) {
                
             System.out.println(rs.getInt(1) + " " + rs.getString(2));
             Statement st2 = con.createStatement();
             String sqlUpdate = "update mDetails set stock=" + rs.getInt(1) + " where mName='" + rs.getString(2) + "'";
                
             System.out.println(sqlUpdate);
             st2.executeUpdate(sqlUpdate);

             }*/
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println("done");

    }

    public void updateStock() {
        try {
            String sqlUpS = "select count(*),medicineName from mIdName group by medicineName order by medicineId";
            //q="select * from mDetails";
            rs = st.executeQuery(sqlUpS);

            //ResultSet lrs=st.executeQuery(q);
            int row = 0;
            /*while(rs.next())
             row++;
             System.out.println(row);*/
            while (rs.next()) {

                System.out.println(rs.getInt(1) + " " + rs.getString(2));
                Statement st2 = con.createStatement();
                String sqlUpdate = "update mDetails set stock=" + rs.getInt(1) + " where mName='" + rs.getString(2) + "'";

                System.out.println(sqlUpdate);
                st2.executeUpdate(sqlUpdate);

            }
        } catch (SQLException ex) {
            Logger.getLogger(MyDb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteAll() {
        try {
            String sqlAll = "delete from outDated";
            st.executeUpdate(sqlAll);
        } catch (SQLException ex) {
            Logger.getLogger(MyDb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ResultSet login(String u, String p) {
        try {
            String sqlLogin = "select * from login where username='" + u + "'and password='" + p + "'";
            rs = st.executeQuery(sqlLogin);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(MyDb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void delete(int id) {
        String sqlDelete = "delete from mIdName where medicineId='" + id + "'";
        try {
            st.executeUpdate(sqlDelete);
            updateStock();

            System.out.println("deleted");
        } catch (SQLException ex) {
            Logger.getLogger(MyDb.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void deleteStaff(int id) {
        try {

            st.executeUpdate("delete from staff where sId=" + id);
            System.out.println("nikal diya saale ko");
        } catch (SQLException ex) {
            Logger.getLogger(MyDb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteGen(String query) {
        try {
            Statement st2 = con.createStatement();
            st2.executeUpdate(query);

            updateStock();
        } catch (SQLException ex) {
            Logger.getLogger(MyDb.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public ResultSet getSales()
    {
        try {
            String sqlSales="select * from sales";
            rs=st.executeQuery(sqlSales);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(MyDb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ResultSet search(String query) {
        try {
            //String sqlSearch="select * from mDetails where mName='"+name+"'";
            rs = st.executeQuery(query);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(MyDb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void insertStaff(String name, int age, int pay) {
        try {
            String sqlInsert = "insert into staff(name,age,pay) values('" + name + "'," + age + "," + pay + ")";
            st.executeUpdate(sqlInsert);
        } catch (SQLException ex) {
            Logger.getLogger(MyDb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void checkExpiry() throws SQLException {
        String sqlCheck = "select * from mIdName;";
        rs = st.executeQuery(sqlCheck);
        while (rs.next()) {
            try {
                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                Date date = new Date();
                String dmy = df.format(date);
                Date d = df.parse(rs.getString(3));

                try {
                    if (d.before(df.parse(dmy))) {
                        int id = Integer.parseInt(rs.getString(1));
                        //dont forget to complete
                        String sqlInsertIntoOutdated = "insert into outDated values(" + id + ",'" + rs.getString(2) + "','" + rs.getString(3) + "')";
                        //delete(id);
                        st.executeUpdate(sqlInsertIntoOutdated);
                        delete(id);
                        System.out.println(d);
                        System.out.println(id);
                    }
                } catch (NumberFormatException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (ParseException ex) {
                Logger.getLogger(MyDb.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

}
